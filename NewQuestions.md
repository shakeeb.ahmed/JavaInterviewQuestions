# The behavior questions on developer position:
 
### 1. Can you just briefly explain about your experience. 
 
### 2. If you get a chance to improve on some process in your current project what would be that. 
 
### 3. What’s extra step and above your duty work you did while working on a critical issue. 
 
### 4. Have done any automation such that it was very useful in systems. 
 
### 5. Do you lead a team and how do you plan your works. 
 
### 6. Any work that you did and felt that you have accomplished in very effective way. 
### 7. Introduce yourself
### 8. Can you describe a challenging task you have performed and how you handled it?
### 9. What planning do you do for a production deployment?
### 10. There were few questions related to my work as I answered their questions. Don’t recall all of them.
### 11. what’s your thought on peer programing ?
### 12. They asked me some Junit question , and what do you know about Test Driven development ?
### 13. Apart from development , what other roles do you perform in your current project ?
### 14. what methodologies are you using or familiar with ?

 
# Technical on developer interview. 
 
### 1. Method overloading and overriding simple and direct explanation
 
### 2. Difference between inner and outer join
Both inner and outer joins are used to combine rows from two or more tables into a single result.  

Inner joins don’t include non-matching rows; whereas, outer joins do include them.
 
### 3. Steps you consider and check when you want improve the performance of the system. 
Here are a few frequently used performance goals for typical web applications:

* Average application response time
* Average concurrent users must the system support
* Expected requests per second during peak load

Code Level Optimization
* Using StringBuilder for String Concatenation
* Avoid Recursion

JDBC Performance
* Statement Caching by using prepared statements
* Use Explain query on table to check the performance and create indexes if necessary.

Architectural Improvements
* Implement caching for non changing values.

### 4. What you meant by Mockito.
Mockito is a mocking framework, JAVA-based library that is used for effective unit testing of JAVA applications. Mockito is used to mock interfaces so that a dummy functionality can be added to a mock interface that can be used in unit testing. 

### 5. Which version of java are you using? Depending on the answer, can you tell differences between your and previous versions (ex: 7 and 6)
	#### Enhancements in Java SE 7 
		* Binary Literals
		* Strings in switch Statement
		* Try with Resources
		* Multiple Exception Handling
		* underscore in literals
		* Type Inference for Generic Instance Creation using Diamond Syntax
		* Improved Compiler Warnings and Errors When Using Non-Reifiable Formal Parameters with Varargs Methods
	#### Enhancements in Java SE 6
		* No language changes were introduced in Java SE 6.

### 6. Can you tell how is Spring different from plain MVC
### 7. REST web-services use a URI or URL? What is the difference between the two
Rest Web-Service uses URI (Uniform Resource Identifier), URI is string of characters to identify a resource using HTTP Methods.

URL stands for Uniform Resource Locator, and is used to specify addresses on the World Wide Web.


### 8. What is high level design, detailed design and technical design? 
**High Level Design (HLD**) is the overall system design - covering the system architecture and database design.

**Low-level design (LLD**) is a component-level design process that follows a step-by-step refinement process. This process can be used for designing data structures, required software architecture, source code and ultimately, performance algorithms.

A Low Level Design for the example would talk about, which programming languages will be used for the Frontend (php/javascript/angular etc..), which design approach will be used (mvc,oop and like), which API endpoints are used

Technical specifications, at least in the form of a technical design, are part of the design documents, along with, for example, requirements lists, functional designs, user stories, graphics design mockups, usability studies, UML diagrams, business process diagrams, data model specifications, etc.

### 9. What factors do you consider when designing a large enterprise level application?

Aspects of Successful Large Scale System Design

While nothing can guarantee the success of any project, there are five factors that – when kept in mind throughout design and implementation – can help system architects ensure that they haven’t overlooked something important. These five are:

Scalability
Availability
Manageability
Security
Development practices

### 10. Describe an application you were involved in designing.
### 11. How did you create REST web-services?
### 12. How would you define security in web services ?
### 13. Difference between REST Services and JAX-RS
JAX-RS

JAX-RS is a specification for implementing REST web services in Java, currently defined by the JSR-370. It is part of the Java EE technologies, currently defined by the JSR 366.

Jersey (shipped with GlassFish and Payara) is the JAX-RS reference implementation, however there are other implementations such as RESTEasy (shipped with JBoss EAP and WildFly) and Apache CXF (shipped with TomEE and WebSphere).

Spring Framework

The Spring Framework is a full framework that allows you to create Java enterprise applications. The REST capabilities are provided by the Spring MVC module (same module that provides model-view-controller capabilities). It is not a JAX-RS implementation and can be seen as a Spring alternative to the JAX-RS standard.

The Spring ecosystem also provides a wide range of projects for creating enterprise applications, covering persistence, security, integration with social networks, batch processing, etc.


### 14. Explain the flow of Spring-MVC(They told me ,for instance I have Employee Info Application , where I need three pages, Save Employee, Find Employee, Update Employee)
**Step 1**: First request will be received by DispatcherServlet
**Step 2**: DispatcherServlet will take the help of HandlerMapping and get to know the Controller class name associated with the given request
**Step 3**: So request transfer to the Controller, and then controller will process the request by executing appropriate methods and returns ModeAndView object (contains Model data and View name) back to the DispatcherServlet
**Step 4**: Now DispatcherServlet send the model object to the ViewResolver to get the actual view page
**Step 5**: Finally DispatcherServlet will pass the Model object to the View page to display the result
That’s it

### 15. how your current application manage the security concern with Database ?

### 16. Diff between Hashtable and Hashmap ? why one is allowing null and other not ?

### 17. what java version are you using ?
### 18. how do you manage code coverage ?
### 19. Cursor in DB
A cursor is a pointer used to fetch rows from a result set. One can think of a cursor as a data structure that describes the results returned from a SQL SELECT statement. One of the variables in this structure is a pointer to the next record to be fetched from the query results.
### 20. Logical structure in database
### 21. Serialization id in class
The serialVersionUID is used as a version control in a Serializable class. If you do not explicitly declare a serialVersionUID, JVM will do it for you automatically.

The default serialVersionUID computation is highly sensitive to class details and may vary from different JVM implementation

#### Client / Server environment
– Client is using SUN’s JVM in Windows.
– Server is using JRockit in Linux.

The client sends a serializable class with default generated serialVersionUID (e.g 123L) to the server over socket, the server may generate a different serialVersionUID (e.g 124L) during deserialization process, and raises an unexpected InvalidClassExceptions.

### 22. Spring Security how to implement LDAP server
	- Add spring-security-ldap in `pom.xml`
	- Implement WebSecurityConfig by extending WebSecurityConfigurerAdapter and override configure()
	- 
### 23. shallow initialization and deep initialization
### 25. Linux how to schedule the build process
### 26. after deploying in server how u verify that application is running
### 27. How you increase heap size in server
### 28. Deference between JDBC 1 and JDBC 2
### 29. Dependency injection.
### 30. Explain Interface, Abstract. Why we need that. when u will prefer interface when u will prefer abstract. If we same example for interface then he will say we can achieve this with abstract also. Then why you are choosing interface.
### 31. What is annotation. Explain me if I don’t know anything in JAVA.
### 32. What is mutable. String Vs String Buffer. How you do concatenation with both approach. If we say answer, he will question both are similar. 
### 33. What is JVM
### 34. String.valueOf() Vs String.toString()
### 35. Collections - ArrayList Vs Vector. 
### 36. What is legacy. Explain me if I don’t know anything in JAVA.
### 37. Is it possible to run the Java code in higher version (like Java 8), which was compiled in lower version(like java 6). 
### 38. Above question in reverse order.
### 39. Polymorphism (Overloading, Overriding)
### 40. Exception handling - Need to explain in deeper level like how compile is happening.
### 41. throws Vs throw
### 42. Modifier in java
### 43. Design pattern - Singleton
### 44. What is encapsulation
### 45. DB (He will give some scenario and ask to tell query) - Second record from whole set.
### 46. Inner join, left outer join, right outer join
### 47. Procedure
### 48. Rest API. why we need this
### 49. scala experience
### 50. Hibernate - why we need this 
### 51. What is BigData. 
### 52. Security
### 53. What is MVC. why we need this. We can achieve this in some other way why we are specific to this framework approach.
### 54. What are the tools you’ve used so far
### 55. How will you track issues.
### 56. Do you have experience in leading the team
### 57. What the features in Java8

————————————————

### how to use iterator - syntax.
### how jvm knows that it is a marker interface?
### how can we create our own marker interface.
### if marker interfaces are not different dn ny other interface, dn hw does jvm knows d diffrence between clonable and serializable interface.
### Difference between synchronizing  a class and synchronizing an object.
### Benefits of String immutability.
### When we prefer to use treemap? Is treemap slow? In what scenarios?
### what is d basic use of mvc frameworks? how they help?
### difference between arrays and arraylist.
### Advantages of generics.
### Is it mandatory to have .ser extention of serialized files.
### How to implement linked list in java.
### How can set properties be achieved with any other data structure.
### Difference between jdk, jre, jvm.
### Difference between `@required` and `@autowired`.
### Difference between context and config.
### JMS messaging system.
### What do you mean by local independent objects?
### Java memory model.
### What is jsp plug-in used for?
### Which is default log file?
### What is catalina?
### implemented example of serialization and deserialization.
### What if hashcode returns same value for all the keys to be added in a map?
### What is the difference between HashSet and LinkedHashSet?
### How is hashset stored in a memory.
### What to do in a POJO to be used in a multithreaded environment ?
### Design patterns - what all design patterns you know? What are the real world examples.
### Why we use Struts, Maven, SONAR.
### diffrence between arraylist and linkedlist. Their big(O) notation. Which should be used when.
### Design principles
### What are futures
### What is a reetrant lock
