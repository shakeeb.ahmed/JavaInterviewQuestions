### Difference between java & j2ee
Java is a language, based on OOPS.  J2EE is a set of specifications, having a suite of enterprise technologies like JMS, Servlets, JSP, EJB, which are implemented using Java.

Java is a language, j2ee is a set of libraries. All d apis we use come under j2ee.

Java EE, i.e. Java Enterprise Edition. This was originally known as Java 2 Platform, Enterprise Edition or J2EE. The name was eventually changed to Java Platform, Enterprise Edition

or

Java EE in version 5. Java EE is mainly used for applications which run on servers, such as web sites.
Java ME, i.e. Java Micro Edition. It is mainly used for applications which run on resource constrained devices (small scale devices) like cell phones, most commonly games.

Java EE or J2EE, as it was previously known, is basically an extension of the Java SE, which is a platform for development and deployment of portable applications for desktop and server environments. It is essentially based on the Java programming language.

Java EE provides an API and runtime environment for developing and running enterprise software, including network and web services. The API is also helpful for object-relational mapping, distributed and multi-tier architectures, and web services.

Java EE provide those APIs that helps in developing enterprise applications/ distributed applications which are hosted on servers. Few APIs are :-
* Servlets
* JSP
* JDBC
* JTA
* JPA
* JMS



Webopedia lists some of the key features and services of J2EE as:

At the client tier, J2EE supports pure HTML, as well as Java applets or applications. It relies on Java Server Pages and servlet code to create HTML or other formatted data for the client.
Enterprise JavaBeans (EJBs) provide another layer where the platform's logic is stored. An EJB server provides functions such as threading, concurrency, security and memory management. These services are transparent to the author.
Java Database Connectivity (JDBC), which is the Java equivalent to ODBC, is the standard interface for Java databases.
The Java servlet API enhances consistency for developers without requiring a graphical user interface.

### What does class.forName do?
* It only loads the class file.
### Difference between object oriented programing language and object based programing language.
* Object based programing language follow all features of OOPs except inheritance. Javascript and vbscript are its examples
### Can we overload main method?
* Yes
### Does constructor return any value?
* Yes, current class instance.
### Why can final object be modified?
* `final` simply makes the object reference unchangeable. The object it points to is not immutable by doing this. INSTANCE can never refer to another object, but the object it refers to may change state.

It means that if your final variable is a reference type (i.e. not a primitive like int), then it's only the reference that cannot be changed. It cannot be made to refer to a different object, but the fields of the object it refers to can still be changed, if the class allows it. For example:

`final StringBuffer s = new StringBuffer();`

The content of the StringBuffer can still be changed arbitrarily:

`s.append("something");` But you cannot say: `s = null;` or `s = anotherBuffer;`

One is optimization where by declaring a variable as final allows the value to be memoized.

Another scenario where you would use a final variable is when an inner class within a method needs to access a variable in the declaring method. The following code illustrates this:

```
public void foo() {
  final int x = 1;
  new Runnable() {
    @Override
    public void run() {
        int i = x;
    }
  };
}
```
If x is not declared as "final" the code will result in a compilation error. The exact reason for needing to be "final" is because the new class instance can outlive the method invocation and hence needs its own instance of x. So as to avoid having multiple copies of a mutable variable within the same scope, the variable must be declared final so that it cannot be changed.

### Can we initialize blank final variable?
Blank final variable in Java is a final variable which is not initialized while declaration, instead they are initialized on constructor. Java compiler will complain if blank final variable is not initialized during construction. If you have more than one constructor or overloaded constructor in your class then blank final variable must be initialized in all of them, failing to do so is a compile time error in Java.

### What is a ‘prototype’ design pattern ? -
 when ever a request is made to instantiate an object, it creates a shalow clone of that object and assign wherever required.
### How does set removes duplicate elements? i.e (what is its functionality to do so.) -
using `equals()`.

### Do we call hashcode explicitly in equals method?
 No
### What is a ‘fail-safe’ iterator? Which collection type implements it?
They are fail-safe because they never fails(never throw exception) - which is actually not good.
ConcurrentHashMap, CopyOnWriteArrayList implements fail-safe iterator
### Which data structure you will use for reversing a string? (you cannot use any java api)
stack
### Real world example of a linked list
train.
### Why we need to call `start()` to start a thread? Why it can’t be done by calling `run()` method? -
Runnable is just an interface. A class implementing Runnable is nothing special, it just has a run method.
The `start()` method tells the JVM that it needs to create a system specific thread. After creating the system resource, it passes the Runnable object to it to execute its `run()` method.
Thread#start calls a natively implemented method that creates a separate thread and calls Thread's run method, executing the code in the new thread.
Causes this thread to begin execution; the Java Virtual Machine calls the run method of this thread.
The result is that two threads are running concurrently: the current thread (which returns from the call to the start method) and the other thread (which executes its run method).
It is never legal to start a thread more than once. In particular, a thread may not be restarted once it has completed execution.
Throws:
`IllegalThreadStateException` if the thread was already started.
See also:
run()
stop()
source code of Thread class’ start method
    ` public synchronized void  start() {`

This method is not invoked for the main method thread or "system" group threads created/set up by the VM. Any new functionality added to this method in the future may have to also be added to the VM. A zero status value corresponds to state "NEW".

```
if (threadStatus != 0)
            throw new IllegalThreadStateException();
         group.add(this);
         start0();
         if (stopBeforeStart) {
             stop0(throwableFromStop);
         }
     }
```

// calling a natively implemented method start0
    ` private native void  start0();`

### There are two threads, if a thread gets into a synchronized method of an object o. Can another thread access the non synchronized method of same object o.
Yes, ofcourse . As only synchronized blocks or methods are blocked while object locking.
### Custom Immutable class.
* Don't provide "setter" methods — methods that modify fields or objects referred to by fields.
* Make all fields final and private.
* Don't allow subclasses to override methods. The simplest way to do this is to declare the class as final. A more sophisticated approach is to make the constructor private and construct instances in factory methods.
* If the instance fields include references to mutable objects, don't allow those objects to be changed:
* Don't provide methods that modify the mutable objects.
* Don't share references to the mutable objects. Never store references to external, mutable objects passed to the constructor; if necessary, create copies, and store references to the copies. Similarly, create copies of your internal mutable objects when necessary to avoid returning the originals in your methods.
http://docs.oracle.com/javase/tutorial/essential/concurrency/imstrat.html

### Custom checked and unchecked exception classes.
 to create custom checked exception - extend Exception class
and to create custom unchecked or runtime exception class - extend RunTimeException.

### Difference between `iterator` and `enumeration`.
* Another similarity between Iterator and Enumeration in Java is that  functionality of Enumeration interface is duplicated by the Iterator interface.
* Only major difference is `iterator` has a remove() method while Enumeration doesn't. Enumeration acts as Read-only interface, because it has the methods only to traverse and fetch the objects, whereas by using Iterator we can manipulate the objects like adding and removing the objects from collection e.g. Arraylist.
* Also Iterator is more secure and safe as it  does not allow other thread to modify the collection object while some thread is iterating over it and throws `ConcurrentModificationException`. This is by far most important fact for me for deciding between Iterator vs Enumeration in Java.

### Serializable vs Externalization in Java
1. In case of Serializable, default serialization process is used. while in case of Externalizable custom Serialization process is used which is implemented by application.
2. JVM gives call back to `readExternel()` and `writeExternal()` of `java.io.Externalizalbe` interface for restoring and writing objects into persistence.
3. Externalizable interface provides complete control of serialization process to application.
4. `readExternal()` and `writeExternal()` supersede any specific implementation of writeObject and readObject methods.
5. Though Externalizable provides complete control, it also presents challenges to serialize super type state and take care of default values in case of transient variable and static variables in Java. If used correctly Externalizable interface can improve performance of serialization process.

### There is an array of length 99. There are integers 0-100 which are inserted randomly. How can you identify the left element?
Adding 0 to 100 values using n(n+1)/2
And add these given numbers. U will get the left no by subtraction.

### There is an array with 0s and non 0s integers. sort the array in such way that 0s are on one side and non 0s are on other side.
Take a linked list, one points to left and another pointer points to right.
Traverse the 2 pointers, until a 1 s found in left or 0 is found in right, if found interchange those.

### Can A be serialized? Explain the complete procedure.

```
class B{
  int a;
}
class A extends B implements serializable{
  transient int a;
  static String b;
  int c;
  B b;      
}
```
Yes, only if a no-argument constructor of B can be called.

### Can static variables be serialized?
NO, static variables are not serialized.

### What is Serialization?
Ans) Serializable is a marker interface. When an object has to be transferred over a network ( typically through rmi or EJB) or to persist the state of an object to a file, the object Class needs to implement Serializable interface. Implementing this interface will allow the object converted into bytestream and transfer over a network.

### What is use of serialVersionUID?
During object serialization, the default Java serialization mechanism writes the metadata about the object, which includes the class name, field names and types, and superclass. This class definition is stored as a part of the serialized object. This stored metadata enables the deserialization process to reconstitute the objects and map the stream data into the class attributes with the appropriate type
Everytime an object is serialized the java serialization mechanism automatically computes a hash value. `ObjectStreamClass`'s `computeSerialVersionUID()` method passes the class name, sorted member names, modifiers, and interfaces to the secure hash algorithm (SHA), which returns a hash value.The serialVersionUID is also called suid.
So when the serilaize object is retrieved , the JVM first evaluates the suid of the serialized class and compares the suid value with the one of the object. If the suid values match then the object is said to be compatible with the class and hence it is de-serialized. If not InvalidClassException exception is thrown.

Changes to a serializable class can be compatible or incompatible. Following is the list of changes which are compatible:

#### Add fields
* Change a field from static to non-static
* Change a field from transient to non-transient
* Add classes to the object tree

List of incompatible changes:

#### Delete fields
* Change class hierarchy
* Change non-static to static
* Change non-transient to transient
* Change type of a primitive field

So, if no suid is present, inspite of making compatible changes, jvm generates new suid thus resulting in an exception if prior release version object is used .

The only way to get rid of the exception is to recompile and deploy the application again.

If we explicitly mention the sUid using the statement:

`private final static long serialVersionUID = <integer value>`

then if any of the metioned compatible changes are made the class need not to be recompiled. But for incompatible changes there is no other way than to compile again.

### What is the need of Serialization?
The serialization is used :-

* To send state of one or more object’s state over the network through a socket.
* To save the state of an object in a file.
* An object’s state needs to be manipulated as a stream of bytes.

### Other than Serialization what are the different approach to make object Serializable?
Besides the Serializable interface, at least three alternate approaches can serialize Java objects:

* For object serialization, instead of implementing the Serializable interface, a developer can implement the Externalizable interface, which extends Serializable. By implementing Externalizable, a developer is responsible for implementing the `writeExternal()` and `readExternal()` methods. As a result, a developer has sole control over reading and writing the serialized objects.
* XML serialization is an often-used approach for data interchange. This approach lags runtime performance when compared with Java serialization, both in terms of the size of the object and the processing time. With a speedier XML parser, the performance gap with respect to the processing time narrows. Nonetheless, XML serialization provides a more malleable solution when faced with changes in the serializable object.
* Finally, consider a "roll-your-own" serialization approach. You can write an object's content directly via either the ObjectOutputStream or the DataOutputStream. While this approach is more involved in its initial implementation, it offers the greatest flexibility and extensibility. In addition, this approach provides a performance advantage over Java serialization.

### Do we need to implement any method of Serializable interface to make an object serializable?
No. Serializable is a Marker Interface. It does not have any methods.

### What happens if the object to be serialized includes the references to other serializable objects?
If the object to be serialized includes references to the other objects, then all those object’s state also will be saved as the part of the serialized state of the object in question. The whole object graph of the object to be serialized will be saved during serialization automatically provided all the objects included in the object’s graph are serializable.

### What happens if an object is serializable but it includes a reference to a non-serializable object?
If you try to serialize an object of a class which implements serializable, but the object includes a reference to an non-serializable class then a `NotSerializableException` will be thrown at runtime.

```
  public class NonSerial { 
      //This is a non-serializable  class 
  }
  public class MyClass implements Serializable { 
    private static final long serialVersionUID = 1L; 
    private NonSerial nonSerial; 
    MyClass(NonSerial  nonSerial){ 
      this.nonSerial = nonSerial; 
    } 
    public static void main(String [] args)  { 
      NonSerial nonSer = new NonSerial(); 
      MyClass c = new MyClass(nonSer); 
      try { 
       FileOutputStream fs = new FileOutputStream("test1.ser"); 
       ObjectOutputStream os = new ObjectOutputStream(fs); 
       os.writeObject(c); 
       os.close(); 
      } catch (Exception e) {  e.printStackTrace(); } 
      try { 
        FileInputStream fis = new FileInputStream("test1.ser"); 
        ObjectInputStream ois = new ObjectInputStream(fis); 
        c = (MyClass)  ois.readObject(); 
        ois.close(); 
      } catch (Exception e) { 
        e.printStackTrace(); 
      } 
     } 
    }     
    
 ```
On execution of above code following exception will be thrown;
```
java.io.NotSerializableException: NonSerial 
at java.io.ObjectOutputStream.writeObject0(ObjectOutputStream.java) 
```

### Are the static variables saved as the part of serialization?
No. The static variables belong to the class are not the part of the state of the object so they are not saved as the part of serialized object.

### What is a transient variable?
These variables are not included in the process of serialization and are not the part of the object’s serialized state.

### What will be the value of transient variable after de-serialization?
It’s default value. e.g. if the transient variable in question is an int, it’s value after deserialization will be zero.
```
public class TestTransientVal implements Serializable { 
  private static final long serialVersionUID =  -22L; 
  private String name; 
  transient private int age; 
TestTransientVal(int age, String  name) { 
  this.age = age; 
  this.name = name; 
} 
public static void main(String [] args)  { 
 TestTransientVal c = new TestTransientVal(1,"ONE"); 
 System.out.println("Before serialization:" + c.name + " "+ c.age); 
 try { 
   FileOutputStream fs =new FileOutputStream("testTransient.ser");
   ObjectOutputStream os = new ObjectOutputStream(fs); 
   os.writeObject(c); 
   os.close(); 
 } catch (Exception e) {  e.printStackTrace(); } 
   try { 
     FileInputStream fis =new FileInputStream("testTransient.ser");
     ObjectInputStream ois =new ObjectInputStream(fis); 
     c = (TestTransientVal)  ois.readObject(); 
     ois.close(); 
   } catch (Exception e) {  e.printStackTrace(); } 
    System.out.println("After  de-serialization:" + c.name +" "+ c.age); 
 } 
}
```
Result of executing above piece of code –

Before serialization: - Value of non-transient variable ONE Value of transient variable 1 
After de-serialization:- Value of non-transient variable ONE Value of transient variable 0

#### Explanation – 
The transient variable is not saved as the part of the state of the serailized variable, it’s value after de-serialization is it’s default value.

### Does the order in which the value of the transient variables and the state of the object using the defaultWriteObject() method are saved during serialization matter?
Yes, while restoring the object’s state the transient variables and the serializable variables that are stored must be restored in the same order in which they were saved.

### How can one customize the Serialization process? or What is the purpose of implementing the writeObject() and readObject() method?
When you want to store the transient variables state as a part of the serialized object at the time of serialization the class must implement the following methods –
```
private void wrtiteObject(ObjectOutputStream outStream) {
  //code to save the transient variables state 
  //as a part of serialized  object
}
private void readObject(ObjectInputStream inStream) {
 //code to read the transient variables state 
//and assign it to the  de-serialized object
}

public class TestCustomizedSerialization implements Serializable { 
 private static final long serialVersionUID =-22L; 
 private String noOfSerVar; 
 transient private int noOfTranVar; 
 TestCustomizedSerialization(int noOfTranVar, String  noOfSerVar) { 
    this.noOfTranVar = noOfTranVar; 
    this.noOfSerVar = noOfSerVar; 
  } 
  private void writeObject(ObjectOutputStream os) { 
    try { 
      os.defaultWriteObject(); 
      os.writeInt(noOfTranVar); 
    } catch (Exception e) {  e.printStackTrace(); } 
  } 
private void readObject(ObjectInputStream is) { 
 try { 
  is.defaultReadObject(); 
   int noOfTransients =  (is.readInt()); 
 } catch (Exception e) { 
   e.printStackTrace(); } 
 } 
public int getNoOfTranVar() { 
return noOfTranVar; 
}
```
The value of transient variable ‘noOfTranVar’ is saved as part of the serialized object manually by implementing writeObject() and restored by implementing readObject().
The normal serializable variables are saved and restored by calling defaultWriteObject() and defaultReadObject()respectively. These methods perform the normal serialization and de-sirialization process for the object to be saved or restored respectively.

### If a class is serializable but its superclass in not, what will be the state of the instance variables inherited from super class after deserialization?

The values of the instance variables inherited from superclass will be reset to the values they were given during the original construction of the object as the non-serializable super-class constructor will run.

E.g.
```
public class ChildSerializable extends ParentNonSerializable implements Serializable {
  private static final long serialVersionUID = 1L; 
  String color; 
  ChildSerializable() { 
    this.noOfWheels = 8; 
    this.color = "blue"; 
  } 
}

public class SubSerialSuperNotSerial { 
  public static void main(String [] args)  { 
    ChildSerializable c = new ChildSerializable(); 
      System.out.println("Before : -  " +  c.noOfWheels + " "+ c.color); 
    try { 
      FileOutputStream fs = new FileOutputStream("superNotSerail.ser"); 
      ObjectOutputStream os = new ObjectOutputStream(fs); 
      os.writeObject(c); 
      os.close(); 
    } catch (Exception e) {  e.printStackTrace(); } 
    try { 
      FileInputStream fis = new FileInputStream("superNotSerail.ser"); 
      ObjectInputStream ois = new ObjectInputStream(fis); 
      c = (ChildSerializable) ois.readObject(); 
      ois.close(); 
    } catch (Exception e) {  e.printStackTrace(); } 
      System.out.println("After :-  " +  c.noOfWheels + " "+ c.color); 
  } 
} 
```
  Result  on executing above code –
  Before : - 8 blue 
  After :- 4 blue
  
The instance variable ‘noOfWheels’ is inherited from superclass which is not serializable. Therefore while restoring it the non-serializable superclass constructor runs and its value is set to 8 and is not same as the value saved during serialization which is 4.

### How to achieve deep cloning?
Other common solution to the deep copy problem is to use Java Object Serialization (JOS). The idea is simple: Write the object to an array using JOSâ€™s ObjectOutputStream and then use ObjectInputStream to reconsstruct a copy of the object. The result will be a completely distinct object, with completely distinct referenced objects. JOS takes care of all of the details: superclass fields, following object graphs, and handling repeated references to the same object within the graph.
It will only work when the object being copied, as well as all of the other objects references directly or indirectly by the object, are serializable. (In other words, they must implement `java.io.Serializable`.)

Fortunately it is often sufficient to simply declare that a given class implements java.io.Serializable and let Javaâ€™s default serialization mechanisms do their thing. Java Object Serialization is slow, and using it to make a deep copy requires both serializing and deserializing.

### Which exception is thrown when
```
class Employee implements Serializable{
Department d;				// not serializable
}
```

java.io.NotSerializableException: Department.

### Can we retrieve java deserialize java object multiple times? Or does deserializing the same object twice actually create two instances in memory?
Yes, deserialization creates a new instance of your object, and deserializing several times will create several instances.
### Difference between error and exception.
Here is my list of notable difference between Error vs Exception in Java.some examples of Error in Java are java.lang.OutOfMemoryError or Java.lang.NoClassDefFoundError and java.lang.UnSupportedClassVersionError.

1. As I said earlier, Main difference on Error vs Exception is that Error is not meant to catch as even if you catch it you can not recover from it. For example during OutOfMemoryError, if you catch it you will get it again because Garbage Collector (GC) may not be able to free memory in first place. On the other hand Exception can be caught and handled properly.
2. Error are often fatal in nature and recovery from Error is not possible which is different in case of Exception which may not be fatal in all cases.
3. Unlike Error, Exception is generally divided into two categories e.g.checked and unchecked Exceptions. Checked Exception has special place in Java programming language and require a mandatory try catch finally code block to handle it. On the other hand Unchecked Exception, which are subclass of RuntimeException mostly represent programming errors. Most common example of unchecked exception is NullPointerException in Java.
4. Similar to unchecked Exception, Error in Java are also unchecked.Compiler will not throw compile time error.

### Can we catch error using Throwable in catch block?
It is possible to catch Throwable. And yes, you will also catch instances of java.lang.Error which is a problem when it comes to e.g. OutOfMemoryError's. In general I'd not catch Throwable's. If you have to, you should do it at the highest place of your call stack, e.g. the main method (you may want to catch it, log it, and rethrow it). It does not make sense to catch events you're not able to handle (e.g. OutOfMemoryError).

### Hierarchy of Exception.

### What is difference between ClassNotFoundException and NoClassDefFoundError?
A ClassNotFoundException is thrown when the reported class is not found by the ClassLoader in the CLASSPATH. It could also mean that the class in question is trying to be loaded from another class which was loaded in a parent classloader and hence the class from the child classloader is not visible.
Consider if NoClassDefFoundError occurs which is something like
java.lang.NoClassDefFoundError
src/com/TestClass
does not mean that the TestClass class is not in the CLASSPATH. It means that the class TestClass was found by the ClassLoader however when trying to load the class, it ran into an error reading the class definition. This typically happens when the class in question has static blocks or members which use a Class that's not found by the ClassLoader. So to find the culprit, view the source of the class in question (TestClass in this case) and look for code using static blocks or static members.

### What if we override equals but do not override `hashcode()`?
Factually, When we don't use hash map or hash set, it shouldn't really matter if we override `hashcode()`. But it is recommended to override both if any one of them is overide as per the contract between `equals()` and `hashcode()` in Object class.  In the case of Object, the javadoc of equals is pretty clear:
Note that it is generally necessary to override the hashCode method whenever this method is overridden, so as to maintain the general contract for the hashCode method, which states that equal objects must have equal hash codes.
So unless you have a real design reason not to override hashcode, the default decision should be to follow the parent class contract and override neither or both.

TOMCAT
======

### why do you need singleton?

Situations where singletons are useful usually involve resources that need to be initialized only once or of which you can logically only have one instance without running into trouble related to concurrent access. Like it or not, such resources exist and singletons are usually the cleanest way of integrating them. If you have a dependency injecting container, you don't get exposed to them directly since the container is in charge of the singleton instead of your application code.
It leads to whole new argument...................................


### Does easymock provides blackbox testing or white box testing?
 whitebox
### How to synchronize Hashmap.
`Collections.synchronizedMap(hashmap)`.
### Explain any Multithreading scenario in your application.
### Why you use EasyMock. -
It provides white box testing. To mock the object to test the unit in isolation.
### How to ensure that main thread is the last thread exiting.
Using wait,notify or join
eg :

### Why can an Object member variable not be both final and volatile in Java?
volatile only has relevance to modifications of the variable itself, not the object it refers to. It makes no sense to have a final volatile field because final fields cannot be modified. Just declare the field final and it should be fine.

### What is pass by value & pass by reference? In java we only have pass by value thing, So, how do we achieve pass by reference concept in java?
Pass by value in java means passing a copy of the value to be passed. Pass by reference in java means the passing the address itself. In Java the arguments are always passed by value. Java only supports pass by value.
With Java objects, the object reference itself is passed by value and so both the original reference and parameter copy both refer to the same Java object. Java primitives too are passed by value.

### package for java tagext -
javax.servlet.jsp.tagext
### package for wrapper classes -
java.lang
### How to specify a role in deployment descriptor?
### getRequestDispatcher takes relative or absolute path as an argument?
###  Why we cannot put `this()` and `super()` both inside a constrauctor.
This is because super calls super class constructor, and this will call current class constructor which in turn calls super class constructor. And by this, u end up calling super class constructor twice.

### Why `super()` or `this()` must be the first statement in a constructor?
So that object is used only after complete instantiation. Half constructed object or not constructed object is not used anyway. Java was designed to make programmer's life easier. Java detects potential errors at compile time so that programmer doesn't run into "Run time errors”.
If this would not be a restriction, Programmers could easily do mistakes of using the object before getting instantiated. i.e writing code before super() or this().

### How to get the 3 last element from a list when size of list is not know in one iteration.
Take 2 pointers(pointing to a node that is referencing to a node). one is pointing to first node. and another is pointing to 3rd node. while traversing put a check if the address part of a node is null. For the last node, address part has to be null. So, first pointer is pointing to the 3rd last node.

### How can i restrict a user from serializing my class but for some reason i have implemented serializable interface.
You can prevent serialization at runtime by any of several ways:

By adding a member variable of a type that does not implement Serializable (as jefflub's answer suggests),

 or

by implementing the method private void writeObject(java.io.ObjectOutputStream out) throws IOException (with that exact signature) and throwing an IOException in that method, a version number, called a serialVersionUID, which is used during de-serialization to verify that the sender and receiver of a serialized object have loaded classes for that object that are compatible with respect to serialization.

### If my singleton class is serialize then its singleton importance is violated as another object is created while serializing. How can i avoid it.
By implementing readResolve object.

```
public class ASingleton implements Serializable{
	/**
	 *
	 */
	private static final long serialVersionUID = 7389627834048911045L;
	private static ASingleton obj;
	privateingleton(){}
	public s AStatic ASingleton getInstance(){
		if(obj == null){
			obj = new ASingleton();
		}
		return obj;
	}

	private Object readResolve() throws ObjectStreamException {
	    return obj;
	}
}
```

It works - But only if the singleton class is serialized and de-serialized in same scope.
i.e in some scenarios, if singleton class is serialized and application is stopped. And after starting the application it is de-serialized it will return null.
At the time of de-serialization, readResolve is called. You can easily control what to be returned using that method.
### what is tokenizer?
Its a legacy class used just for compatibility purpose. Its use is discouraged nowadays. This class do not even recognize nd skip comments.
Its methods do not distinguish between identifiers, numbers, and quoted strings.
The set of delimiters (the characters that separate tokens) may be specified either at creation time or on a per-token basis.
An instance of StringTokenizer behaves in one of two ways, depending on whether it was created with the returnDelims flag having the value true or false:
If the flag is false, delimiter characters serve to separate tokens. A token is a maximal sequence of consecutive characters that are not delimiters.
If the flag is true, delimiter characters are themselves considered to be tokens. A token is thus either one delimiter character, or a maximal sequence of consecutive characters that are not delimiters.
### What's new in java 8?
#### Streams
The changes to collections are driving a lot of the other changes in the language. The main component of making those collections better is a new collection called a "stream." This is not to be confused with java.io package's InputStream and OutputStream -- it's a separate concept altogether.

Streams are not meant to replace ArrayLists or other collections. They are only meant to make manipulating the data easier and faster. A stream is a one-time-use Object. Once it has been traversed, it cannot be traversed again.

#### Functional interfaces
Java 8 will have a new feature called functional interfaces. Basically, default methods are added to an interface and do not have to be overridden in the interface implementation. These methods can be run directly from the interface.

This was done for backward compatibility for your collections in your interfaces. It is to solve the problem of allowing the Stream to be put into an interface without having to change all of the classes to implement the new method. Basically, create a default method in the interface, and all the classes that implement the interface can use the Stream (or whatever is put into the default method). If the default method is not correct for the implementation, it can be overridden in the implementer.

### what is the use of hashcode nd how it is saved in memory?
If you don’t override `hashcode()` then the default implementation in Object class will be used by collections. This implementation gives different values for different objects, even if they are equal according to the `equals()` method
The array index is calculated by taking into account key’s hashcode value and the size of the array.

### why are there two ways of creating thread, if always implementing runnable is preferred.
Sometimes you may have to implement Runnable as well as subclass Thread. For instance, if creating a subclass of Thread that can execute more than one Runnable. This is typically the case when implementing a thread pool.

### Difference between serailization and externalization.
* For a Serializable class, Object Serialization can automatically save and restore fields of each class of an object and automatically handle classes that evolve by adding fields or supertypes. A serializable class can declare which of its fields are saved or restored, and write and read optional values and objects.
*  For an Externalizable class, Object Serialization delegates to the class complete control over in external format and how the state of the supertype(s) is saved and restored.
1.11 The Externalizable Interface
For Externalizable objects, only the identity of the class of the object is saved by the container; the class must save and restore the contents. The Externalizable interface is defined as follows:

```
package java.io;

public interface Externalizable extends Serializable
{
    public void writeExternal(ObjectOutput out)
        throws IOException;

    public void readExternal(ObjectInput in)
        throws IOException, java.lang.ClassNotFoundException;
}
```

The class of an Externalizable object must do the following:
*  Implement the java.io.Externalizable interface
*  Implement a writeExternal method to save the state of the object
(It must explicitly coordinate with its supertype to save its state.)
*  Implement a readExternal method to read the data written by the writeExternal method from the stream and restore the state of the object
(It must explicitly coordinate with the supertype to save its state.)
*  Have the writeExternal and readExternal methods be solely responsible for the format, if an externally defined format is written

Note - The writeExternal and readExternal methods are public and raise the risk that a client may be able to write or read information in the object other than by using its methods and fields. These methods must be used only when the information held by the object is not sensitive or when exposing it does not present a security risk.
*  Have a public no-arg constructor

An Externalizable class can optionally define the following methods:
*  A writeReplace method to allow a class to nominate a replacement object to be written to the stream
*  A readResolve method to allow a class to designate a replacement object for the object just read from the stream

### What kind of data is stored in a file while serialization.
byte array?

### significance of creating object as final.
If you indicate any variable to be final, it means that you don’t want the value it contains in the memory to change. In case of primitive types, that value represented by variables are the actual values. In case of object types, the values in memory are references to the object and not the actual objects.

### difference between wrapper classes and primitive datatypes.
Use the primitive type unless you have no other choice. The fact that it’s not nullable will prevent many bugs. And they’re also faster.

Besides collections, wrapper types are typically used to represent a nullable value (for example, coming from a database nullable column).
The wrapper classes are immutable, and therefore have no setter methods.Every time you want to change its value a new object would have to be created. This would create unnecessary objects. The int primitive just represents the bits of the number. You are not creating any new object. Therefore I would suggest using the primitive when not using collections.
The wrapper classes can be quite helpful when your system produces a level of uncertainty that must be accounted for. For example using Boolean in a packet capture program such as wireshark, would allow you to pass along a null value if you were unable to determine if a packet was modified from the sender. Specifically in the case when you are able to determine other packet manipulations.


### what is the use of singleton class?
Singleton is a nice design pattern. If in your solution some object has only one instance and you want to model that in your design then you should use singleton pattern. For example if you are modelling a PC in the software there can be only one instance of a PC with respect to your running program. As Jon Skeet said java.lang.Runtime is modelled as a singleton because for all the java objects that are loaded and running inside a java runtime there is only one instance of runtime.
Some examples:
* Hardware access
* Database connections
* Config files

Again lot of times it is used for the wrong reasons. Never create a singleton so that you can easily access the object (like Object::instance() ) from anywhere without passing the object around. The is the worst use I have ever come across.

### What is an immutable class?

Immutable class is a class which once created, it’s contents can not be changed. Immutable objects are the objects whose state can not be changed once constructed. e.g. String class

### How to create an immutable class?

To create an immutable class following steps should be followed:

* Create a final class.
* Set the values of properties using constructor only.
* Make the properties of the class final and private
* Do not provide any setters for these properties.
* If the instance fields include references to mutable objects, * don’t allow those objects to be changed:
* Don’t provide methods that modify the mutable objects.
* Don’t share references to the mutable objects. Never store references to external, mutable objects passed to the constructor; if necessary, create copies, and store references to the copies. Similarly, create copies of your internal mutable objects when necessary to avoid returning the originals in your methods.
E.g.

```
public final class FinalPersonClass {

      private final String name;
      private final int age;

      public FinalPersonClass(final String name, final int age) {
            super();
            this.name = name;
            this.age = age;
      }
      public int getAge() {
            return age;
      }
      public String getName() {
            return name;
      }

}
```

### if a string is final. How the following code is valid.

```
String str = “xyz”;
str = “abc”;
```

In ds case also, state of d object does not change. A new object is created. Xyz and abc are 2 objects.

### Immutable objects are automatically thread-safe –true/false?

True. Since the state of the immutable objects can not be changed once they are created they are automatically synchronized/thread-safe.

### Which classes in java are immutable?

All wrapper classes in java.lang are immutable –
String, Integer, Boolean, Character, Byte, Short, Long, Float, Double, BigDecimal, BigInteger

### What are the advantages of immutability?

The advantages are:
1. Immutable objects are automatically thread-safe, the overhead caused due to use of synchronisation is avoided.
2. Once created the state of the immutable object can not be changed so there is no possibility of them getting into an inconsistent state.
3. The references to the immutable objects can be easily shared or cached without having to copy or clone them as there state can not be changed ever after construction.
4. The best use of the immutable objects is as the keys of a map.

### can we override or overload main method.
You can very well overload a main method but jvm will only use that static main method with string array arguments as an application entry point. U cannot override static methods. So u can’t override main method.


### Why string final in java?

* Imagine StringPool facility without making string immutable , its not possible at all because in case of string pool one string object/literal e.g. “Test” has referenced by many reference variables , so if any one of them change the value others will be automatically gets affected i.e. lets say

```
String A = “Test”
String B = “Test”
```

Now String B called “Test”.toUpperCase() which change the same object into “TEST” , so A will also be “TEST” which is not desirable.

* String has been widely used as parameter for many Java classes e.g. for opening network connection, you can pass hostname and port number as string , you can pass database URL as string for opening database connection, you can open any file in Java by passing name of file as argument to File I/O classes. In case, if String is not immutable, this would lead serious security threat , I mean some one can access to any file for which he has authorization, and then can change the file name either deliberately or accidentally and gain access of those file. Because of immutability, you don’t need to worry about those kind of threats. This reason also gel with, Why String is final in Java, by making java.lang.String final, Java designer ensured that no one overrides any behavior of String class.

* Since String is immutable it can safely shared between many threads ,which is very important for multithreaded programming and to avoid any synchronization issues in Java, Immutability also makes String instance thread-safe in Java, means you don’t need to synchronize String operation externally. Another important point to note about String is memory leak caused by SubString, which is not a thread related issues but something to be aware of.

* Another reason of Why String is immutable in Java is to allow String to cache its hashcode , being immutable String in Java caches its hashcode, and do not calculate every time we call hashcode method of String, which makes it very fast as hashmap key to be used in hashmap in Java.  This one is also suggested by  Jaroslav Sedlacek in comments below. In short because String is immutable, no one can change its contents once created which guarantees hashCode of String to be same on multiple invocation.

### Why Use Generics?
In a nutshell, generics enable types (classes and interfaces) to be parameters when defining classes, interfaces and methods. Much like the more familiar formal parameters used in method declarations, type parameters provide a way for you to re-use the same code with different inputs. The difference is that the inputs to formal parameters are values, while the inputs to type parameters are types.
Code that uses generics has many benefits over non-generic code:
Stronger type checks at compile time.
A Java compiler applies strong type checking to generic code and issues errors if the code violates type safety. Fixing compile-time errors is easier than fixing runtime errors, which can be difficult to find.

Elimination of casts.
The following code snippet without generics requires casting:

```
List list = new ArrayList();
list.add(“hello”);
String s = (String) list.get(0);
```

When re-written to use generics, the code does not require casting:

```
List<String> list = new ArrayList<String>();
list.add(“hello”);
String s = list.get(0);   // no cast
```

Enabling programmers to implement generic algorithms.
By using generics, programmers can implement generic algorithms that work on collections of different types, can be customized, and are type safe and easier to read.

### different ways of creating objects in java.
New operator, serialization, reflection, cloning.
There are four different ways to create objects in java:
* Using new keyword
This is the most common way to create an object in java. Almost 99% of objects are created in this way.

`MyObject object = new MyObject();`

* Using Class.forName()
If we know the name of the class & if it has a public default constructor we can create an object in this way.

```
MyObject object = (MyObject) Class.forName(“subin.rnd.MyObject”).newInstance();
```
* Using `clone()`

The `clone()` can be used to create a copy of an existing object.

```
MyObject anotherObject = new MyObject(); MyObject object = anotherObject.clone();
```

* Using object deserialization
Object deserialization is nothing but creating an object from its serialized form.

```
ObjectInputStream inStream = new ObjectInputStream(anInputStream );
MyObject object = (MyObject) inStream.readObject();
```

### Why is verify used for in unit testing?
EasyMock doesn’t just check that expected methods are called with the right arguments. It can also verify that you call those methods and only those methods, in the right order. This checking is not performed by default. To turn it on, call EasyMock.verify(mock) at the end of your test method.

### How to make a class variable thread safe?
by making it volatile.

### How to create a custom exception class.
Extend exception for custom checked exeptions and runtimeexception for custom unchecked exception. To use any of the ways of in built classes you need to define the constructor in custom class and call super constructor.
Eg.

```
public class MyException extends Exception {
  public MyException(String message) {
    super(message);
  }
  public MyException() {
    super();
  }
}
```

### Binary search.
it is for sorted array.
In this we compare the value with middle element. If value is smaller we go for the array before middle element and if it is larger dn we go for the array after middle element. It involves recursive calls.

### can class be declared private or protected?
As you know default is for package level access and protected is for package level plus non-package classes but which extends this class(Point to be noted here is you can extend the class only if it is visible!). lets put it in this way:

protected top-level class would be visible to classes in its package.
now making it visible outside the package (subclasses ) is bit confusing and tricky.

### Which classes should be allowed to inherit our protected class?
If all the classes are allowed to subclass then it will be similar to public access specifier.
If none then it is similar to Default.
Since there is no way to restrict this class being subclassed by only few classes ( we cannot restrict class being inherited by only few classes out of all the available classes in a package/outsite of a package or do we??!!), there is no use of protected access specifiers for top level classes. Hence it is not allowed.
Private - as ds class will nt be visible to anyone. So it wont be of ny use.
But inner classes can be made protected or private.

### can we override main method?
No, it is a static method. But we can overload it with different parameters and it would be another normal static method.
### can static methods be overloaded?
Yes, but we cannot overload methods if dey just differ by static keyword. 2 or more static methods can be overloaded. Difference lies in input parameters.
### how can we create custom collection?
Extending abstract implementation of a particular collection you need. Like for custom list, extend abstractList class.
### what all required in a tree to be a Binary Search Tree?
The common properties of Binary Search Tree are as follows:
* The left subtree of a node contains only nodes with keys less than the node's key.
* The right subtree of a node contains only nodes with keys greater than the node's key.
* The left and right subtree each must also be a binary search tree.
* Each node can have up to two successor nodes.
* There must be no duplicate nodes.
* A unique path exists from the root to every other node.

### rules of overriding methods?
* The argument list should be exactly the same as that of the overridden method.
* The return type should be the same or a subtype of the return type declared in the original overridden method in the superclass.
* The access level cannot be more restrictive than the overridden method's access level. For example: if the superclass method is declared public then the overridding method in the sub class cannot be either private or protected.
* Instance methods can be overridden only if they are inherited by the subclass.
* A method declared final cannot be overridden.
* A method declared static cannot be overridden but can be re-declared.
* If a method cannot be inherited, then it cannot be overridden.
* A subclass within the same package as the instance's superclass can override any superclass method that is not declared private or final.
* A subclass in a different package can only override the non-final methods declared public or protected.
* An overriding method can throw any uncheck exceptions, regardless of whether the overridden method throws exceptions or not. However the overriding method should not throw checked exceptions that are new or broader than the ones declared by the overridden method. The overriding method can throw narrower or fewer exceptions than the overridden method.
* Constructors cannot be overridden.

### what is java order of operations?
Left to right.

```
int i = 5;
int j = 10;
System.out.println("a"+i+ j); // a510 - when both side operators are same, java executes from left to right i.e. "a" + 5 and dn "a5"+10.
System.out.println("a" + i - j); // compile error. + has precedence over -
System.out.println("a" + i * j); // a50
```

### How is cloning and serialization different than using “new” and reflection?
When you clone an object, that means that you are dealing with something that lies in a different part of memory from the original object. Yes, they might have the same properties, but they are two different pointers with two different blocks of memory.

When you deserialize an object then an object exists which did not exist before. Even if you serialize and then immediately unserialize, it will exist independently from the original object.

The very important point here is that in object deserialization there is no constructor involved in the process -- that's why it is a distinct way to create an object. This is also true of cloning -- the method Object.clone creates a new object by JVM magic, again not involving any constructors. There is in fact much greater difference between these two ways on the one hand and new and reflection on the other, since reflection is just a slightly different way to invoke the plain-vanilla object instantiation involving a specific constructor.

### how are runtime exception are handeled?
Its simple just like normal exceptions, you can put try catch block around d code.

NullPointerExceptions are usually the sign of a missing null check. So, instead of catching it like this, you should add the apropriate null check to be sure to not throw the exception.

But sometimes, it is appropiate to handle RunTimeExceptions. For example, when you cannot modify the code to add the null check at the appropriate place, or when the exception is something other than a NullPointerException.

Your example of handling exceptions is terrible. Doing so, you lose the stack trace and precise information about the problem. And you are actually not solving it as you will probably trigger another NullPointerException in a different place, and get misleading information about what happened and how to solve it.

### What is J2EE
 J2EE is a platform-independent, Java-centric environment from Sun for developing, building and deploying Web-based enterprise applications online. The J2EE platform consists of a set of services, APIs, and protocols that provide the functionality for developing multitiered, Web-based applications.

### can is also called Inner Class.
1) Nested static class doesn’t need reference of Outer class, but Non-static nested class or Inner class requires Outer class reference.

2) Inner class(or non-static nested class) can access both static and non-static members of Outer class. A static class cannot access non-static members of the Outer class. It can access only static members of Outer class.

3) An instance of Inner class cannot be created without an instance of outer class and an Inner class can reference data and methods defined in Outer class in which it nests, so we don’t need to pass reference of an object to the constructor of the Inner class. For this reason Inner classes can make program simple and concise.

```
/* Java program to demonstrate how to implement static and non-static
   classes in a java program. */
class OuterClass{
   private static String msg = “GeeksForGeeks”;

   // Static nested class
   public static class NestedStaticClass{

       // Only static members of Outer class is directly accessible in nested
       // static class
       public void printMessage() {

         // Try making ‘message’ a non-static variable, there will be
         // compiler error
         System.out.println(“Message from nested static class: “ + msg);
       }
    }

    // non-static nested class - also called Inner class
    public class InnerClass{

       // Both static and non-static members of Outer class are accessible in
       // this Inner class
       public void display(){
          System.out.println(“Message from non-static nested class: “+ msg);
       }
    }
}
class Main
{
    // How to create instance of static and non static nested class?
    public static void main(String args[]){

       // create instance of nested Static class
       OuterClass.NestedStaticClass printer = new OuterClass.NestedStaticClass();

       // call non static method of nested static class
       printer.printMessage();

       // In order to create instance of Inner class we need an Outer class
       // instance. Let us create Outer class instance for creating
       // non-static nested class
       OuterClass outer = new OuterClass();
       OuterClass.InnerClass inner  = outer.new InnerClass();

       // calling non-static method of Inner class
       inner.display();

       // we can also combine above steps in one step to create instance of
       // Inner class
       OuterClass.InnerClass innerObject = new OuterClass().new InnerClass();

       // similarly we can now call Inner class method
       innerObject.display();
    }
}
```

Output:

Message from nested static class: GeeksForGeeks

Message from non-static nested class: GeeksForGeeks

Message from non-static nested class: GeeksForGeeks a class be static? What is a static class.

### are static imports only for static methods.
yes, static imports allows you to access static members of an external class.
few points worth remembering about static import in Java :

1) Static import statements are written as "import static" in code and not "static import".

2) If you import two static fields with same name explicitly e.g. Integer.MAX_VALUE and Long.MAX_VALUE then Java will throw compile time error. But if other static modifier is not imported explicitly e.g. you have imported `java.lang.Long.*`,
`MAX_VALUE` will refer to `Integer.MAX_VALUE`.

3) Static import doesn't improve readability as expected, as many Java programmer prefer Integer.MAX_VALUE which is clear that which MAX_VALUE are you referring.

4) You can apply static import statement not only on static fields but also on static methods in Java.

### collections and exceptions are in which package?
exception is in java.lang
Checked exceptions are in corresponding packages like sqlexception is in sql package, ioexception is in io package.

### what is getInstance()?
Method getInstance() is called factory method. It is used for singleton class creation. That means only one instance of that class will be created and others will get reference of that class.

### why use generics??
Code that uses generics has many benefits over non-generic code:
Stronger type checks at compile time.
A Java compiler applies strong type checking to generic code and issues errors if the code violates type safety. Fixing compile-time errors is easier than fixing runtime errors, which can be difficult to find.

Elimination of casts.
The following code snippet without generics requires casting:

```
List list = new ArrayList();
list.add("hello");
String s = (String) list.get(0);
When re-written to use generics, the code does not require casting:
List<String> list = new ArrayList<String>();
list.add("hello");
String s = list.get(0);   // no cast
```

Enabling programmers to implement generic algorithms.
By using generics, programmers can implement generic algorithms that work on collections of different types, can be customized, and are type safe and easier to read.

### differences between stack and heap memory in Java:

1) Main difference between heap and stack is that stack memory is used to store local variables and function call, while heap memory is used to store objects in Java. No matter, where object is created in code e.g. as member variable, local variable or class variable,  they are always created inside heap space in Java.

2) Each Thread in Java has there own stack which can be specified using -Xss JVM parameter, similarly you can also specify heap size of Java program using JVM option -Xms and -Xmx where -Xms is starting size of heap and -Xmx is maximum size of java heap. to learn more about JVM options see my post 10 JVM option Java programmer should know.

3) If there is no memory left in stack for storing function call or local variable, JVM will throw java.lang.StackOverFlowError, while if there is no more heap space for creating object, JVM will throw java.lang.OutOfMemoryError: Java Heap Space. Read more about how to deal with java.lang.OutOfMemoryError  in my post 2 ways to solve OutOfMemoryError in Java.

4) If you are using Recursion, on which method calls itself, You can quickly fill up stack memory. Another difference between stack and heap is that size of stack memory is lot lesser than size of  heap memory in Java.

5) Variables stored in stacks are only visible to the owner Thread, while objects created in heap are visible to all thread. In other words stack memory is kind of private memory of Java Threads, while heap memory is shared among all threads.

### Sorting ArrayList of strings in descending order in Java

```
Collections.sort(unsortedList, Collections.reverseOrder());
System.err.println(“Sorted ArrayList in Java -Descending order : “ + unsortedList);
```

### Can a transient variable be made final?
An instance member field declared as final could also be transient, but if so, you would face a problem a little bit difficult to solve: As the field is transient, its state would not be serialized, it implies that, when you deserialize the object you would have to initialize the field manually, however, as it is declared final, the compiler would complain about it.

Q
Serializable is called a marker, but only because classes like ObjectOutputStream and ObjectInputStream use the reflection API to check for it. There is no special magic there; if you wanted to implement your own alternative serialization strategy, you could also create your own interface and use that as the marker you wished to respect. Although, you should consider whether it would actually be better design to use the existing Java marker (Serializable). I believe some other alternatives to the built-in Object stream do this.

If you merely wish to take complete control over how your objects are serialized into bytes, you can do so without rewriting the ObjectXXXStream classes, by using Serializable subclass Externalizable. And in fact, this is commonly done, since the automatic serialization afforded by Serializable ungracefully dies with the smallest change to the SerialVersionUID, which, unless supplied, is calculated automatically from various signatures of the class - making Serializable unsuitable for must persistent storage, since most kinds of changes (i.e. during a routine upgrade) to the underlying class will prevent reloading the serialized data.

### can we override static method?
We can re-declare static methods with same signature in subclass, but it is not considered overriding as there won’t be any run-time polymorphism. Hence the answer is ‘No’.
If a derived class defines a static method with same signature as a static method in base class, the method in the derived class hides the method in the base class
Derived extends base

```
 Base obj1 = new Derived();
       // As per overriding rules this should call to class Derive’s static
       // overridden method. Since static method can not be overridden, it
       // calls Base’s display()
       obj1.display();
```

### does abstract class has constructor?
Yes,
Now if we say we can not create instance of abstract class then why do Java adds constructor in abstract class. One of the reason which make sense is,  when any class extend abstract class, constructor of sub class will invoke constructor of super class either implicitly or explicitly. This chaining of constructors is one of the reason abstract class can have constructors in Java.

### is it required to have abstract method inside abstract class?
The subject of this post and the body ask two different questions:
Should it have at least one abstract member?

Is it necessary to have at least one abstract member?

The answer to #2 is definitively no.

The answer to #1 is subjective and a matter of style.

Personally I would say yes. If your intent is to prevent a class (with no abstract methods) from being instantiated, the best way to handle this is with a private protected constructor, not by marking it abstract.

### diffrencebetween arraylist and vector.
### What’s the difference between Serialization and simply store the object on disk?

Serializable is marker interface which denotes that the object of your class can be converted into byte stream and eventually back into java object if required. Initially you might think that every class should be serializable but that’s not correct consider

Input- and OutputStreams which have some file handle to read from. That file handle is closed when the stream becomes unavailable. So serialization at this instance doesn’t make sense; and de-serialization would never restore that handle.

So this should answer why marking as Serializable is required?

Now implementing methods which define how your object should be written or read; that should be defined by you and hence you need all those stream objects and readObject, writeObject methods. Hope this gives you a bit more understanding of this concept.

You cannot assign a Parent class to subclass. The reason is subclass might have methods or properties that do not exist in Parent and hence you won’t be able to call child class methods.

### Db connection example

```
public static void main(String[] args)
{
String url = “jdbc:mysql://localhost:3306/“;
 String dbName = “demo”
String driver = “com.mysql.jdbc.Driver”;
String userName = “root”;
String password = “mypasswd”;
 try {
Class.forName(driver).newInstance();
Connection conn = DriverManager.getConnection(url+dbName,userName,password);
conn.close();
} catch (Exception e) {
 e.printStackTrace();
}
 }
```

### Java properties file example

```
public static void main(String[] args) {

	Properties prop = new Properties();
	OutputStream output = null;

	try {

		output = new FileOutputStream(“config.properties”);

		// set the properties value
		prop.setProperty(“database”, “localhost”);
		prop.setProperty(“dbuser”, “mkyong”);
		prop.setProperty(“dbpassword”, “password”);

		// save properties to project root folder
		prop.store(output, null);

	} catch (IOException io) {
		io.printStackTrace();
	} finally {
		if (output != null) {
			try {
				output.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

	}

```

load properties file

```
public static void main( String[] args ){

    	Properties prop = new Properties();
    	InputStream input = null;

    	try {

    		input = new FileInputStream(“config.properties”);

		// load a properties file
		prop.load(input);

		// get the property value and print it out
		System.out.println(prop.getProperty(“database”));
		System.out.println(prop.getProperty(“dbuser”));
    	        System.out.println(prop.getProperty(“dbpassword”));

    	} catch (IOException ex) {
    		ex.printStackTrace();
        } finally{
        	if(input!=null){
        		try {
				input.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
        	}
        }
```

Do You Need to Write a main() Method in a JUnit Test Case Class?
The right answer to this question is “No”. But many developers do write a main() method in a JUnit test case class to call a JUnit test runner to run all tests defined in this class. This is not recommended, because you can always call a JUnit runner to run a test case class as a system command.
If you want to know how to call a JUnit runner in a main() method, you can this code included in the sample test case class listed in the previous question. This code is provided by Varun Chopra and valid for JUnit 3.8.

```
public static void main(String[] args) {
  junit.textui.TestRunner.run(DirListerTest.class);
}
```

junit.textui.TestRunner.run() method takes test class name as argument. Using reflection, this method finds all class methods whose name starts with test. So it will find following 3 methods:
 testCreateLogFile()
  testExists()
  testGetChildList()

It will execute each of the 3 methods in unpredictable sequence (hence test case methods should be independent of each other) and give the result in console.

### Can we start a thread twice

No. After starting a thread, it can never be started again. If you does so, an IllegalThreadStateException is thrown. In such case, thread will run once but for second time, it will throw exception.

Let’s understand it by the example given below:

```
public class TestThreadTwice1 extends Thread{
 public void run(){
   System.out.println(“running…”);
 }
 public static void main(String args[]){
  TestThreadTwice1 t1=new TestThreadTwice1();
  t1.start();
  t1.start();
 }
}
```

Test it Now
       running
       Exception in thread “main” java.lang.IllegalThreadStateException

### What is difference between wait() and sleep() method?

1) The wait() method is defined in Object class.	The sleep() method is defined in Thread class.

2) wait() method releases the lock.	The sleep() method doesn’t releases the lock.

### diffrencebetween Static synchronization and object synchronization

With object synchroziation, 2 diffrenct threads on 2 diffrenct objects can interfere but sometimes, no threads should interfere. At that time we use static synchronization.

Static synch is applied only on static methods

If you make any static method as synchronized, the lock will be on the class not on object.

static synchronization
Problem without static synchronization

Suppose there are two objects of a shared class(e.g. Table) named object1 and object2.In case of synchronized method and synchronized block there cannot be interference between t1 and t2 or t3 and t4 because t1 and t2 both refers to a common object that have a single lock.But there can be interference between t1 and t3 or t2 and t4 because t1 acquires another lock and t3 acquires another lock.I want no interference between t1 and t3 or t2 and t4.Static synchronization solves this problem.