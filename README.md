# ## Object Oriented Principles

### What is abstraction?
Abstraction in Java or Object oriented programming is a way to segregate implementation from an interface and one of the five fundamentals along with Encapsulation, Inheritance, Polymorphism, Class, and Object.  Abstraction in Java is achieved by  using interface and abstract class in Java.

### What is abstract class in Java?
An abstract class is something which is incomplete and you can not create an instance of the abstract class. If you want to use it you need to make it complete or concrete by extending it.

### Abstraction : Things to Remember
1. Use abstraction if you know something needs to be in class but the implementation of that varies. Abstraction is actually resulting of thought process and it really need good experience of both domain and Object oriented analysis and design to come up with good abstraction for your project.

2. In Java, you can not create an instance of the abstract class using the new operator, its compiler error. Though abstract class can have a constructor.

3. abstract is a keyword in Java, which can be used with both class and method.  Abstract class can contain both abstract and concrete method. An abstract method doesn't have the body, just declaration.

4. A class automatically becomes abstract class when any of its methods declared as abstract.

5. abstract method doesn't have method body.

6. In Java, a variable can not be made abstract , its only class or methods which would be abstract.

7. If a class extends an abstract class or interface it has to provide implementation to all its abstract method to be a concrete class. alternatively, this class can also be abstract.

## What is polymorphism in Java?
Polymorphism is the capability of a method to do different things based on the object that it is acting upon. In other words, polymorphism allows you define one interface and have multiple implementations.

#### Static Polymorphism: (compile time polymorphism or static binding)
In Java, static polymorphism is achieved through method overloading.

#### Dynamic Polymorphism: (run time polymorphism or Dynamic binding)
In Java, dynamic polymorphism is achieved through method  overriding. (This needs inheritance)

### Encapsulation
Encapsulation in Java is a mechanism of wrapping the data (variables) and code acting on the data (methods) together as a single unit. In encapsulation, the variables of a class will be hidden from other classes, and can be accessed only through the methods of their current class. Therefore, it is also known as data hiding.

### Inheritance in Java
Inheritance is an important pillar of OOP(Object Oriented Programming). It is the mechanism in java by which one class is allow to inherit the features(fields and methods) of another class.

#### Important terminology:

##### Super Class:
The class whose features are inherited is known as super class(or a base class or a parent class).
##### Sub Class:
The class that inherits the other class is known as sub class(or a derived class, extended class, or child class). The subclass can add its own fields and methods in addition to the superclass fields and methods.
##### Reusability:
Inheritance supports the concept of “reusability”, i.e. when we want to create a new class and there is already a class that includes some of the code that we want, we can derive our new class from the existing class. By doing this, we are reusing the fields and methods of the existing class.

### Explain about your project and tell me your role on current project and explain your recent critical project which generate more revenue to customer.

### Importance of XSD in Web Services and what it would contain and how significant XSD used in Web Services.
XSD defines a schema which is a definition of how an XML document can be structured. You can use it to check that a given XML document is valid and follows the rules you've laid out in the schema.

WSDL is an XML document that describes a web service. It shows which operations are available and how data should be structured to send to those operations.

WSDL documents have an associated XSD that show what is valid to put in a WSDL document.

### why do we need to override hashcode() and equals() method and what would happen if we are not overriding them.

Or

### Is it mandatory to implement both equals and hashcode together?

Or 

### Why always override `hashcode()` if overriding `equals()`?

In Java, every object has access to the `equals()` method because it is inherited from the Object class. However, this default implementation just simply compares the memory addresses of the objects. You can override the default implementation of the `equals()` method defined in `java.lang.Object`. If you override the `equals()`, you MUST also override `hashcode()`. Otherwise a violation of the general contract for `Object.hashCode()` will occur, which can have unexpected repercussions when your class is in conjunction with all hash-based collections.

Here is the contract, copied from the `java.lang.Object` specialization:

`public int hashcode()`

Returns a hash code value for the object. This method is supported for the benefit of hashtables such as those provided by `java.util.Hashtable`.

The general contract of hashCode is:

* Whenever it is invoked on the same object more than once during an execution of a Java application, the hashCode method must consistently return the same integer, provided no information used in equals comparisons on the object is modified. This integer need not remain consistent from one execution of an application to another execution of the same application.  

* If two objects are equal according to the equals(Object) method, then calling the hashCode method on each of the two objects must produce the same integer result.  

* It is not required that if two objects are unequal according to the equals(java.lang.Object) method, then calling the hashCode method on each of the two objects must produce distinct integer results. However, the programmer should be aware that producing distinct integer results for unequal objects may improve the performance of hashtables.

As much as is reasonably practical, the hashCode method defined by class Object does return distinct integers for distinct objects. (This is typically implemented by converting the internal address of the object into an integer, but this implementation technique is not required by the JavaTM programming language.)

The default implementation of `equals()` method checks to see if the two objects have the same identity. Similarly, the default implementation of the `hashcode()` method returns an integer based on the object's identity and is not based on the values of instance (and class) variables of the object. No matter how many times the values of its instance variables (data fields) change, the hash code calculated by the default hashCode implementation does not change during the life of the object.
Consider the following code, we have overridden `equals()` method to check if two objects are equal based on the values of their instance variables. Two objects may be stored at different memory addresses but may still be equal base on their instance variable.

```
public class CustomerID {
 private long crmID;
 private int nameSpace;

 public CustomerID(long crmID, int nameSpace) {
   super();
   this.crmID = crmID;
   this.nameSpace = nameSpace;
 }

 public boolean equals(Object obj) {
   //null instanceof Object will always return false
   if (!(obj instanceof CustomerID))
     return false;
   if (obj == this)
     return true;
   return  this.crmID == ((CustomerID) obj).crmID &&
           this.nameSpace == ((CustomerID) obj).nameSpace;
 }

 public static void main(String[] args) {
   Map m = new HashMap();
   m.put(new CustomerID(2345891234L,0),"Jeff Smith");
   System.out.println(m.get(new CustomerID(2345891234L,0)));
 }

}
```
Compile and run the above code, the output result is  `null`

What is wrong? The two instances of CustomerID are logically equal according to the class's equals method. Because the `hashcode()` method is not overridden, these two instances' identities are not in common to the default hashCode implementation. Therefore, the Object.hashCode returns two seemingly random numbers instead of two equal numbers. Such behavior violates "Equal objects must have equal hash codes" rule defined in the hashCode contract.

### Synchronized block vs method

The synchronization is the capability to control the access of multiple threads to shared resources. Without synchronization, it is possible for one thread to modify a shared resource while another thread is in the process of using or updating that resource.

There two synchronization syntax in Java Language. The practical differences are in controlling scope and the monitor. With a synchronized method, the lock is obtained for the duration of the entire method. With synchronized blocks you can specify exactly when the lock is needed.

### Different scopes of bean in Spring and how they differ.

#### 1. singleton (default)
Scopes a single bean definition to a single object instance per Spring IoC container.

#### 2. prototype
Scopes a single bean definition to any number of object instances.

#### 3. request
Scopes a single bean definition to the lifecycle of a single HTTP request; that is each and every HTTP request will have its own instance of a bean created off the back of a single bean definition. Only valid in the context of a web-aware Spring ApplicationContext.

#### 4. session
Scopes a single bean definition to the lifecycle of a HTTP Session. Only valid in the context of a web-aware Spring ApplicationContext.

#### 5. global session
Scopes a single bean definition to the lifecycle of a global HTTP Session. Typically only valid when used in a portlet context. Only valid in the context of a web-aware Spring ApplicationContext.

### oracle: what is index and why do we need to use it.
Indexes are used to quickly locate data without having to search every row in a database table every time a database table is accessed. Indexes can be created using one or more columns of a database table, providing the basis for both rapid random lookups and efficient access of ordered records.

### what is circular dependency in oracle
Records which point to other records are useful in a database. Sometimes these records form a cycle. This might still be useful. The only real annoyance in practice is avoiding violating the constraints.


### What design patterns have you used.

#### Singleton pattern
Singleton pattern is one of the simplest design patterns in Java. This type of design pattern comes under creational pattern as this pattern provides one of the best ways to create an object.

This pattern involves a single class which is responsible to create an object while making sure that only single object gets created. This class provides a way to access its only object which can be accessed directly without need to instantiate the object of the class.


#### Factory pattern
Factory pattern is one of the most used design patterns in Java. This type of design pattern comes under creational pattern as this pattern provides one of the best ways to create an object.

In Factory pattern, we create object without exposing the creation logic to the client and refer to newly created object using a common interface.

FactoryPatternDemo, our demo class will use ShapeFactory to get a Shape object. It will pass information (CIRCLE / RECTANGLE / SQUARE) to ShapeFactory to get the type of object it needs.

#### Builder Pattern
Builder pattern builds a complex object using simple objects and using a step by step approach. This type of design pattern comes under creational pattern as this pattern provides one of the best ways to create an object.

A Builder class builds the final object step by step. This builder is independent of other objects.

#### MVC Pattern
MVC Pattern stands for Model-View-Controller Pattern. This pattern is used to separate application's concerns.

* Model - Model represents an object or JAVA POJO carrying data. It can also have logic to update controller if its data changes.

* View - View represents the visualization of the data that model contains.

* Controller - Controller acts on both model and view. It controls the data flow into model object and updates the view whenever data changes. It keeps view and model separate.

#### DAO pattern
Data Access Object Pattern or DAO pattern is used to separate low level data accessing API or operations from high level business services. Following are the participants in Data Access Object Pattern.

* Data Access Object Interface - This interface defines the standard operations to be performed on a model object(s).

* Data Access Object concrete class - This class implements above interface. This class is responsible to get data from a data source which can be database / xml or any other storage mechanism.

* Model Object or Value Object - This object is simple POJO containing get/set methods to store data retrieved using DAO class.

### What do you check in code review
#### Design
* How does the new code fit with the overall architecture?
* Does the code follow SOLID principles, Domain Driven Design and/or other design paradigms the team favors?
* What design patterns are used in the new code? Are these appropriate?
* If the codebase has a mix of standards or design styles, does this new code follow the current practices? Is the code migrating in the correct direction, or does it follow the example of older code that is due to be phased out?
* Is the code in the right place? For example, if the code is related to Orders, is it in the Order Service?
* Could the new code have reused something in the existing code? Does the new code provide something we can reuse in the existing code? Does the new code introduce duplication? If so, should it be refactored to a more reusable pattern, or is this acceptable at this stage?
* Is the code over-engineered? Does it build for reusability that isn’t required now? How does the team balance considerations of reusability with YAGNI?

#### Readability & Maintainability
* Do the names (of fields, variables, parameters, methods and classes) actually reflect the thing they represent?
* Can I understand what the code does by reading it?
* Can I understand what the tests do?
* Do the tests cover a good subset of cases? Do they cover happy paths and exceptional cases? Are there cases that haven’t been considered?
* Are the exception error messages understandable?
* Are confusing sections of code either documented, commented, or covered by understandable tests (according to team preference)?

#### Functionality
* Does the code actually do what it was supposed to do? If there are automated tests to ensure correctness of the code, do the tests really test the code meets the agreed requirements?
* Does the code look like it contains subtle bugs, like using the wrong variable for a check, or accidentally using an and instead of an or?

### When to use SOAP and when to use REST services
While the SOAP (Simple Object Access Protocol) has been the dominant approach to web service interfaces for a long time, REST (Representational State Transfer) is quickly winning out and now represents over 70% of public APIs.

#### Case 1: Developing a Public API
REST focuses on resource-based (or data-based) operations and inherits its operations (GET, PUT, POST, DELETE) from HTTP. This makes it easy for both developers and web-browsers to consume it, which is beneficial for public APIs where you don’t have control over what’s going on with the consumer. Simplicity is one of the strongest reasons that major companies like Amazon and Google are moving their APIs from SOAP to REST.

#### Case 2: Extensive Back-and-Forth Object Information
APIs used by apps that require a lot of back-and-forth messaging should always use REST. For example, mobile applications. If a user attempts to upload something to a mobile app (say, an image to Instagram) and loses reception, REST allows the process to be retried without major interruption, once the user regains cell service.

However, with SOAP stateful operations, the same type of service would require more initialization and state code. Because REST is stateless, the client context is not stored on the server between requests, giving REST services the ability to be retried independently of one another.

#### Case 3: Your API Requires Quick Developer Response
REST allows easy, quick calls to a URL for fast return responses. The difference between SOAP and REST, in this case, is complexity—-SOAP services require maintaining an open stateful connection with a complex client. REST, in contrast, enables requests that are completely independent of each other. The result is that testing with REST is much simpler.

### How do you design a micro-service
Micro-services architecture as a service‑oriented architecture composed of loosely coupled elements that have bounded contexts.

Loosely coupled means that you can update the services independently; updating one service doesn’t require changing any other services. If you have a bunch of small, specialized services but still have to update them together, they’re not micro-services because they’re not loosely coupled.

* Create a Separate Data Store for Each Microservice
* Keep Code at a Similar Level of Maturity
* Do a Separate Build for Each Microservice
* Deploy in Containers
* Treat Servers as Stateless

### Have you worked on JSF
### How do you check memory leaks
* Start the application and wait until it get to "stable" state, when all the initialization is complete and the application is idle.
* Run the operation suspected of producing a memory leak several times to allow any cache, DB-related initialization to take place.
* Run GC and take memory snapshot.
* Run the operation again. Depending on the complexity of operation and sizes of data that is processed operation may need to be run several to many times.
* Run GC and take memory snapshot.
* Run a diff for 2 snapshots and analyze it.

### Difference between String and StringBuilder.
#### Mutability Difference:

`String` is immutable, if you try to alter their values, another object gets created, whereas `StringBuffer` and `StringBuilder` are mutable so they can change their values.

#### Thread-Safety Difference:

The difference between `StringBuffer` and `StringBuilder` is that `StringBuffer` is thread-safe. So when the application needs to be run only in a single thread then it is better to use `StringBuilder`. `StringBuilder` is more efficient than `StringBuffer`.

### What is Inversion of Control?
Inversion of Control is a principle in software engineering by which the control of objects or portions of a program is transferred to a container or framework. It is most often used in the context of object-oriented programming.

By contrast with traditional programming, in which our custom code makes calls to a library, IoC enables a framework to take control of the flow of a program and make calls to our custom code. In order to make this possible, frameworks are defined through abstractions with additional behavior built in. If we want to add our own behavior, we need to extend the classes of the framework or plugin our own classes.

The advantages of this architecture are:

* decoupling the execution of a task from its implementation
* making it easier to switch between different implementations
* greater modularity of a program
* greater ease in testing a program by isolating a component or mocking its dependencies and allowing components to communicate through contracts

Inversion of Control can be achieved through various mechanisms such as: Strategy design pattern, Service Locator pattern, Factory pattern, and Dependency Injection (DI).

### What is Dependency Injection?
Dependency injection is a pattern through which to implement IoC, where the control being inverted is the setting of object’s dependencies.

The act of connecting objects with other objects, or “injecting” objects into other objects, is done by an assembler rather than by the objects themselves.

Here’s how you would create an object dependency in traditional programming:

```
public class Store {
    private Item item;

    public Store() {
        item = new ItemImpl1();    
    }
}
```

In the example above, you can see that in order to instantiate the item dependency of a Store object you have to specify which implementation of interface Item we will use within the Store class itself.

By using DI, we can rewrite the example without specifying the implementation of Item that we want:

```
public class Store {
    private Item item;
    public Store(Item item) {
        this.item = item;
    }
}
```

The implementation of Item to be injected will then be provided through metadata, as we will see in the examples below.

Both IoC and DI are simple concepts, but have deep implications in the way we structure our systems, so they’re well worth understanding well.

### The Spring IoC Container
An IoC container is a common characteristic of frameworks that implement IoC.

In the Spring framework, the IoC container is represented by the interface ApplicationContext. The Spring container is responsible for instantiating, configuring and assembling objects known as beans, as well as managing their lifecycle.

The Spring framework provides several implementations of the ApplicationContext interface — ClassPathXmlApplicationContext and FileSystemXmlApplicationContext for standalone applications, and WebApplicationContext for web applications.

In order to assemble beans, the container uses configuration metadata, which can be in the form of XML configuration or annotations.

Here is one way to manually instantiate a container:

```
ApplicationContext context
  = new ClassPathXmlApplicationContext("applicationContext.xml");
```


### What is pair programming
The idea is two developers work on the same machine. Both have keyboard and mouse. At any given time one is driver and the other navigator. The roles switch either every hour, or whenever really. The driver codes, the navigator is reading, checking, spell-checking and sanity testing the code, whilst thinking through problems and where to go next. If the driver hits a problem, there are two people to find a solution, and one of the two usually has a good idea.
Other advantages include the fact that where two people have differing specialities, these skills are transferred. Ad-hoc training occurs as one person shows the other some tricks, nice workarounds, etcetera.

The end result is that both developers are fully aware of the code, how it works, and why it was done that way. Chances are the code is better than one developer working alone, as there was somebody watching. It's less likely to contain bugs and hacks and things that cause maintenance problems later.

### SOA architecture
* Service-oriented architecture (SOA) is an evolution of distributed computing based on the request/reply design paradigm for synchronous and asynchronous applications. An application's business logic or individual functions are modularized and presented as services for consumer/client applications. What's key to these services is their loosely coupled nature; i.e., the service interface is independent of the implementation. Application developers or system integrators can build applications by composing one or more services without knowing the services' underlying implementations. For example, a service can be implemented either in .Net or J2EE, and the application consuming the service can be on a different platform or language.

* Service-oriented architectures have the following key characteristics:

* SOA services have self-describing interfaces in platform-independent XML documents. Web Services Description Language (WSDL) is the standard used to describe the services.
* SOA services communicate with messages formally defined via XML Schema (also called XSD). Communication among consumers and providers or services typically happens in heterogeneous environments, with little or no knowledge about the provider. Messages between services can be viewed as key business documents processed in an enterprise.
* SOA services are maintained in the enterprise by a registry that acts as a directory listing. Applications can look up the services in the registry and invoke the service. Universal Description, Definition, and Integration (UDDI) is the standard used for service registry.
* Each SOA service has a quality of service (QoS) associated with it. Some of the key QoS elements are security requirements, such as authentication and authorization, reliable messaging, and policies regarding who can invoke services.

### Hashmap vs Hashtable
1. HashMap is non synchronized. It is not-thread safe and can’t be shared between many threads without proper synchronization code whereas Hashtable is synchronized. It is thread-safe and can be shared with many threads.
2. HashMap allows one null key and multiple null values whereas Hashtable doesn’t allow any null key or value.
3. HashMap is generally preferred over HashTable if thread synchronization is not needed

```
class Ideone
{
    public static void main(String args[])
    {
        //—————hashtable ————————————
        Hashtable<Integer,String> ht=new Hashtable<Integer,String>();
        ht.put(101,” ajay”);
        ht.put(101,”Vijay”);
        ht.put(102,”Ravi”);
        ht.put(103,”Rahul”);
        System.out.println(“——————Hash table———————“);
        for (Map.Entry m:ht.entrySet()) {
            System.out.println(m.getKey()+” “+m.getValue());
        }
 
        //————————hashmap————————————————
        HashMap<Integer,String> hm=new HashMap<Integer,String>();
        hm.put(100,”Amit”);
        hm.put(104,”Amit”);  // hash map allows duplicate values
        hm.put(101,”Vijay”);
        hm.put(102,”Rahul”);
        System.out.println(“—————Hash map—————“);
        for (Map.Entry m:hm.entrySet()) {
            System.out.println(m.getKey()+” “+m.getValue());
        }
    }
}
```

### Why HashTable doesn’t allow null and HashMap does?
To successfully store and retrieve objects from a HashTable, the objects used as keys must implement the hashCode method and the equals method. Since null is not an object, it can’t implement these methods. HashMap is an advanced version and improvement on the Hashtable. HashMap was created later.

### interface vs abstraction

* **Type of methods**: Interface can have only abstract methods. Abstract class can have abstract and non-abstract methods. From Java 8, it can have default and static methods also.
Final Variables: Variables declared in a Java interface are by default final. An abstract class may contain non-final variables.
* **Type of variables**: Abstract class can have final, non-final, static and non-static variables. Interface has only static and final variables.
* **Implementation**: Abstract class can provide the implementation of interface.	Interface can’t provide the implementation of abstract class.
* **Inheritance vs Abstraction**: A Java interface can be implemented using keyword “implements” and abstract class can be extended using keyword “extends”.
* **Multiple implementation**: An interface can extend another Java interface only, an abstract class can extend another Java class and implement multiple Java interfaces.
* **Accessibility of Data Members**: Members of a Java interface are public by default. A Java abstract class can have class members like private, protected, etc.

### Java Garbage Collection

In java, garbage means unreferenced objects.

Garbage Collection is process of reclaiming the runtime unused memory automatically. In other words, it is a way to destroy the unused objects.

To do so, we were using free() function in C language and delete() in C++. But, in java it is performed automatically. So, java provides better memory management.

#### Advantage of Garbage Collection

It makes java memory efficient because garbage collector removes the unreferenced objects from heap memory.
It is automatically done by the garbage collector(a part of JVM) so we don’t need to make extra efforts.
How can an object be unreferenced?

There are many ways:

By nulling the reference
By assigning a reference to another
By annonymous object etc.

* By nulling a reference:

```
Employee e=new Employee();  
e=null;  
```
* By assigning a reference to another:

```
Employee e1=new Employee();  
Employee e2=new Employee();  
e1=e2;//now the first object referred by e1 is available for garbage collection  
```
* By annonymous object:

`new Employee();  `

#### finalize() method

The finalize() method is invoked each time before the object is garbage collected. This method can be used to perform cleanup processing. This method is defined in Object class as:

`protected void finalize(){}  `

Note: The Garbage collector of JVM collects only those objects that are created by new keyword. So if you have created any object without new, you can use finalize method to perform cleanup processing (destroying remaining objects).

#### gc() method

The gc() method is used to invoke the garbage collector to perform cleanup processing. The gc() is found in System and Runtime classes.

`public static void gc(){}  `

### session management in servlet
Since Session management needs to work with all web browsers and also considers user’s security preference, often an identifier i.e. a SessionId is used to keep track of request coming from the same client during a time duration. There are four main ways to manage Session in Java Web application written using Servlet and JSP.

1. URL rewriting
2. Cookies
3. Hidden Form fields
4. HTTPS and SSL


### exceptions handling types
Types of Exception

There are mainly two types of exceptions: checked and unchecked where error is considered as unchecked exception. The sun microsystem says there are three types of exceptions:

* Checked Exception
* Unchecked Exception
* Error

### Difference between checked and unchecked exceptions

1. Checked Exception

The classes that extend Throwable class except RuntimeException and Error are known as checked exceptions e.g.IOException, SQLException etc. Checked exceptions are checked at compile-time.

2. Unchecked Exception

The classes that extend RuntimeException are known as unchecked exceptions e.g. ArithmeticException, NullPointerException, ArrayIndexOutOfBoundsException etc. Unchecked exceptions are not checked at compile-time rather they are checked at runtime.

3. Error

Error is irrecoverable e.g. OutOfMemoryError, VirtualMachineError, AssertionError etc.

### try catch throw

#### UNION
The UNION command is used to select related information from two tables, much like the JOIN command. However, when using the UNION command all selected columns need to be of the same data type. With UNION, only distinct values are selected.

#### UNION ALL
The UNION ALL command is equal to the UNION command, except that UNION ALL selects all values.

### union vs union all
The difference between Union and Union All is that Union All will not eliminate duplicate rows, instead it just pulls all rows from all tables fitting your query specifics and combines them into a table.

### DB normalisation, when would you need to denormalise
#### Normalise
Essentially normalising your schema means breaking it into multiple tables. In a traditional application you might have the following tables; users, categories, pages, profiles, profilemeta, tags, media, pagescategories and so on. Normalisation prevents redundant data and keeps things lean.

You might be familiar with normalisation if you’ve ever had to JOIN multiple tables together to generate a result (a social network profile page is a good example of this). You would have a profiles table, users table, a relationships table and possibly a few other tables to join. If you are using proper indexes, then speed isn’t really an issue as indexed queries will be fast even on a lot of rows.

With normalisation you also prevent content duplication which some would argue is a non-issue given how cheap storage is, but that is besides the point. Every single entity is only stored once, so there is no inconsistency when it comes to data.

#### Denormalise
When you denormalise your data you go the exact opposite as normalisation. Instead of splitting up data into smaller tables which you join, denormalised data means you cram as much data as you can into one table.

Using the social networking example you would replace multiple tables with one user table and have all fields relating to a user in it. Instead of having a users table, a profiles table and a profile meta table, every single possible field is now in the users table.

Your SELECT queries are going to be fast now, but you’ve just opened up a Pandora’s box of pain: nice one. Why? What about bits of data the user hasn’t supplied that are optional? You’ve now got gaping holes in your data and your results are fat.

#### So which one makes more sense?
Academically, most people are taught to choose normalisation and in most cases it makes sense. You shouldn’t worry too much about your database structure in terms of scaling in the early days of a project. You’re unlikely to run into any scaling problems a little more hardware can’t fix and even then, we are talking potentially hundreds of millions of rows.

The general consensus is to normalise your tables first and then go through and denormalise the read heavy parts and leave the write heavy parts normalised. In the beginning choose one or the other, normalisation has the benefit of playing nicely with an MVC application structure opposed to a denormalised structure which can be messy and chaotic if not managed correctly.

### ORM - advantage, disadvantage
Traditionally Programmers used ODBC, JDBC, ADO etc to access database. Developers need to write SQL queries, process the result set and convert the data in the form of objects (Data model). I think most programmers would typically write a function to convert the object to query and result set to object. To overcome these difficulties, ORM provides a mechanism to directly use objects and interact with the database.

#### There are lots of advantages of using ORM
* Database independent. This is the biggest advantage. No need to write code specific to database. I have worked in various products where they maintain separate module / code base for every database and there is lot of effort invested to support multiple database. ORM is a boon.
* There is no need to write SQL queries. Session.saveOrUpdate(entityObject) takes care of insertion in case of Hibernate.
* Takes care of dependencies between tables and does join queries.
* Few ORM libraries has support of caching. Hibernate uses ehcache and provides caching support. This reduces the load from the database and increases the response time.
* Maintains transactions commit and rollback.
* Maintains database connection pool.
* Concurrency support.
* Easy maintenance and increases productivity.

#### Few disadvantages:
* ORM makes life easier but developers will eventually skip learning SQL and database internals.
* There will be some overhead involved using ORM. If the database is accessed directly then developers are having some control and they could fine tune its performance.
* There is a learning curve involved in understanding ORM library. Java, .NET, PHP has better ORM libraries. .NET has support of LINQ, which is a Framework that encompass language-integrated query.
* If your project is using single database and you may need to run some complex queries and fine tune the performance then choose ODBC, JDBC, ADO or similar kind of Data access layers. If not choose ORM as it will make your life easier. But spend some time in understanding the database internals as it will help to take leverage of both ends.

### Would you use an instance variable in a servlet? Why?
The container does not create a new instance of the Servlet class for each request. It reuses an existing one. This is why instance variable are not thread safe.

### Batch job is taking a lot of time to run, whereas the actual query takes very less time. How would you debug?
### what is Domain Name System and how it is working? what will happen when user clicks on web page and how it send request to work center server?
### Performance enhancement
### How do you debug any issue in production
### Have you used multithreading and how
### What all should be present in design doc.